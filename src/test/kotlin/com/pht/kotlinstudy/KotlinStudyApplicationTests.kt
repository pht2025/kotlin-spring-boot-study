package com.pht.kotlinstudy

import com.pht.kotlinstudy.model.BetCount
import com.pht.kotlinstudy.model.Message
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.function.Consumer

class KotlinStudyApplicationTests {

    val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    @Test
    fun localDateTimeTest() {
        val dayBefore = 20L
        val today = LocalDate.now().atStartOfDay()
        val last1 = LocalDate.now().minusDays(15L).atStartOfDay()
        val last2 = LocalDate.now().minusDays(20L).atStartOfDay()
        val last3 = LocalDate.now().minusDays(21L).atStartOfDay()
        val last4 = LocalDate.now().minusDays(30L).atStartOfDay()

        val r1 = last1.plusDays(dayBefore).isAfter(today)
        val r2 = last1.plusDays(dayBefore).isBefore(today)

        val r3 = last2.plusDays(dayBefore).isAfter(today)
        val r4 = last2.plusDays(dayBefore).isBefore(today)
        val r5 = last2.plusDays(dayBefore).isEqual(today)

        val r6 = last3.plusDays(dayBefore).isAfter(today)
        val r7 = last3.plusDays(dayBefore).isBefore(today)

        val r8 = last4.plusDays(dayBefore).isAfter(today)
        val r9 = last4.plusDays(dayBefore).isBefore(today)

        println("last1.plusDays(dayBefore).isAfter(today): $r1")
        println("last1.plusDays(dayBefore).isBefore(today): $r2")
        println()

        println("last2.plusDays(dayBefore).isAfter(today): $r3")
        println("last2.plusDays(dayBefore).isBefore(today): $r4")
        println("last2.plusDays(dayBefore).isEqual(today): $r5")
        println()

        println("last3.plusDays(dayBefore).isAfter(today): $r6")
        println("last3.plusDays(dayBefore).isBefore(today): $r7")
        println()

        println("last4.plusDays(dayBefore).isAfter(today): $r8")
        println("last4.plusDays(dayBefore).isBefore(today): $r9")
    }

    @Test
    fun numberFormatTests() {
        val n1 = 5000000000000000000L
        val r1 = NumberFormat.getInstance().format(n1)
        val r2 = DecimalFormat.getInstance().format(n1)
        println(r1)
        println(r2)

        val r3 = NumberFormat.getInstance().parse(r1)
        println(r3)

        try {
            val r4 = NumberFormat.getInstance().parse("")
            println(r4)
        } catch (e: ParseException) {
            println(e.message)
        }
    }

    @Test
    fun splitAndTrimTest() {
        val input = " wood, falcon\t, sky, forest\n"
        val output = input.trim().split(Regex("\\s*,\\s*")).toTypedArray()
        for (s in output) {
            println("S1 = '$s'")
        }
        val input2 = ""
        val output2 = input2.trim().split(Regex("\\s*,\\s*")).toTypedArray()
        for (s in output2) {
            println("S2 = '$s'")
        }
    }

    @Test
    fun webClientTest() {
        val url = "http://127.0.0.1:8584/v2/user/betCounts/1/2021-01-12T06:39:55"
        val webClient = WebClient.builder()
            .baseUrl(url)
            .defaultHeader("adv-access-token", "C75283491EE20CD61164C98D8D096A29C3698804010A5F417750BD6CA5B7B760")
            .build()
        val block = webClient.get()
            .accept(MediaType.APPLICATION_JSON)
            .retrieve().bodyToFlux(BetCount::class.java)
            .collectList().block()
        block?.forEach { betCount -> sendMessage(betCount) }
    }

    private fun sendMessage(betCount: BetCount) {
        logger.debug("------------------ Send Message. $betCount")
    }
}
