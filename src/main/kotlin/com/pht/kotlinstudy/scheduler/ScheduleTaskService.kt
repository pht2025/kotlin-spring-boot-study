package com.pht.kotlinstudy.scheduler

import com.pht.kotlinstudy.model.PropertyType
import com.pht.kotlinstudy.repository.ExcludeNickRepository
import com.pht.kotlinstudy.repository.GlobalPropertyRepository
import com.pht.kotlinstudy.repository.MessageRepository
import com.pht.kotlinstudy.repository.SendHistoryRepository
import com.pht.kotlinstudy.task.SendCountMessageTask
import com.pht.kotlinstudy.task.SendMoneyMessageTask
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.stereotype.Service
import java.time.Duration
import java.util.concurrent.ScheduledFuture

@Service
class ScheduleTaskService(
    val commonScheduler: ThreadPoolTaskScheduler,
    val messageRepository: MessageRepository,
    val globalPropertyRepository: GlobalPropertyRepository,
    val sendHistoryRepository: SendHistoryRepository,
    val excludeNickRepository: ExcludeNickRepository,
    var environment: Environment
) {
    var sendCountMessageScheduleAtFixedRate: ScheduledFuture<*>? = null
    var sendMoneyMessageScheduleAtFixedRate: ScheduledFuture<*>? = null
    val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    fun startSendCountMessageTask(duration: Duration = Duration.ofSeconds(30)): Boolean {
        stopSendCountMessageTask()
        val countMessage = messageRepository.findByKey(PropertyType.COUNT.name)
        return if (countMessage.isPresent && countMessage.get().onOff == "ON") {
            logger.info("Start send COUNT message schedule task : $duration")
            val sendCountMessageTask = SendCountMessageTask(
                messageRepository, globalPropertyRepository, sendHistoryRepository, excludeNickRepository,
                environment
            )
            sendCountMessageScheduleAtFixedRate = commonScheduler.scheduleAtFixedRate(sendCountMessageTask, duration)
            true
        } else {
            logger.info("Start send COUNT message schedule task OFF : $duration")
            false
        }
    }

    fun stopSendCountMessageTask() {
        logger.info("Stop send COUNT message schedule task")
        sendCountMessageScheduleAtFixedRate?.cancel(false)
        sendCountMessageScheduleAtFixedRate = null
    }

    fun isSendCountMessageRunning(): Boolean {
        return sendCountMessageScheduleAtFixedRate != null
    }

    fun startSendMoneyMessageTask(duration: Duration = Duration.ofSeconds(30)): Boolean {
        stopSendMoneyMessageTask()
        val moneyMessage = messageRepository.findByKey(PropertyType.MONEY.name)
        return if (moneyMessage.isPresent && moneyMessage.get().onOff == "ON") {
            logger.info("Start send MONEY message schedule task : $duration")
            val sendMoneyMessageTask = SendMoneyMessageTask(
                messageRepository, globalPropertyRepository, sendHistoryRepository, excludeNickRepository,
                environment
            )
            sendMoneyMessageScheduleAtFixedRate = commonScheduler.scheduleAtFixedRate(sendMoneyMessageTask, duration)
            true
        } else {
            logger.info("Start send MONEY message schedule task OFF : $duration")
            false
        }
    }

    fun stopSendMoneyMessageTask() {
        logger.info("Stop send MONEY message schedule task")
        sendMoneyMessageScheduleAtFixedRate?.cancel(false)
        sendMoneyMessageScheduleAtFixedRate = null
    }

    fun isSendMoneyMessageRunning(): Boolean {
        return sendMoneyMessageScheduleAtFixedRate != null
    }
}
