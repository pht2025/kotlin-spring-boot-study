package com.pht.kotlinstudy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.EntityListeners

@EntityListeners(AuditingEntityListener::class)
@SpringBootApplication
class KotlinStudyApplication

fun main(args: Array<String>) {
    runApplication<KotlinStudyApplication>(*args)
}
