package com.pht.kotlinstudy.model

data class BetCount(val userId: String, val userNick: String, val count: Int, val createdAt: String?)
