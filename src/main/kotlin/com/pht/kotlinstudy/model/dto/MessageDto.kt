package com.pht.kotlinstudy.model.dto

import com.pht.kotlinstudy.model.GlobalProperty
import com.pht.kotlinstudy.model.PropertyType

class MessageDto {

    data class Request(
        val messageId: Long,
        val message: String,
        val title: String,
        val propertyValue: String,
        val propertyType: PropertyType,
        val dayBefore: Int,
        val onOff: String,
        val excludeNickList: String
    )

    data class Response(val result: Boolean, val excludeNickList: String?, val serverStatus: GlobalProperty.ServerStatus, val onOff: GlobalProperty.OnOff)
}
