package com.pht.kotlinstudy.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
class ExcludeNick(@field:NotNull var nick: String, @field:NotNull @Enumerated(EnumType.STRING) var type: PropertyType) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @CreationTimestamp
    var created: Date? = null

    @UpdateTimestamp
    var updated: Date? = null

    override fun toString(): String {
        return "ExcludeNick(id=$id, nick='$nick', type=$type)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExcludeNick

        if (nick != other.nick) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = nick.hashCode()
        result = 31 * result + (type?.hashCode() ?: 0)
        return result
    }
}
