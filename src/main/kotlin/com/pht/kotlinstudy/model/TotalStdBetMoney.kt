package com.pht.kotlinstudy.model

data class TotalStdBetMoney(val userId: String, val userNick: String, val totalStdBetMoney: Long)
