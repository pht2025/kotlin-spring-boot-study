package com.pht.kotlinstudy.controller

import com.pht.kotlinstudy.Global
import com.pht.kotlinstudy.model.GlobalProperty
import com.pht.kotlinstudy.model.Message
import com.pht.kotlinstudy.model.Property
import com.pht.kotlinstudy.model.PropertyType
import com.pht.kotlinstudy.repository.ExcludeNickRepository
import com.pht.kotlinstudy.repository.GlobalPropertyRepository
import com.pht.kotlinstudy.repository.MessageRepository
import com.pht.kotlinstudy.scheduler.ScheduleTaskService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import reactor.core.publisher.Mono
import java.text.NumberFormat

@Controller
class IndexController(
        val messageRepository: MessageRepository,
        val globalPropertyRepository: GlobalPropertyRepository,
        val excludeNickRepository: ExcludeNickRepository,
        val scheduleTaskService: ScheduleTaskService
) {

    @GetMapping("/")
    fun index(modelAndView: ModelAndView): Mono<ModelAndView> {
        val interval = globalPropertyRepository.findByKey(Global.KEY_INTERVAL).orElse(GlobalProperty())
        val senderNick = globalPropertyRepository.findByKey(Global.KEY_SENDER_NICK).orElse(GlobalProperty())
        val accessToken = globalPropertyRepository.findByKey(Global.KEY_ACCESS_TOKEN).orElse(GlobalProperty())
        val countMessage = messageRepository.findByKey(PropertyType.COUNT.name).orElse(Message())
        val moneyMessage = messageRepository.findByKey(PropertyType.MONEY.name).orElse(Message())
        val countExcludeNickList = excludeNickRepository.findAllByType(PropertyType.COUNT)
        val moneyExcludeNickList = excludeNickRepository.findAllByType(PropertyType.MONEY)
        val countNickList = countExcludeNickList.map { excludeNick -> excludeNick.nick }
        val moneyNickList = moneyExcludeNickList.map { excludeNick -> excludeNick.nick }

        val countProperty = if (countMessage.properties.isEmpty()) {
            Property()
        } else {
            countMessage.properties[0]
        }
        val moneyProperty = if (moneyMessage.properties.isEmpty()) {
            Property()
        } else {
            moneyMessage.properties[0]
        }

        modelAndView.viewName = "index"

        val onOff: GlobalProperty.OnOff
        val serverStatus: GlobalProperty.ServerStatus
        if (scheduleTaskService.isSendCountMessageRunning() || scheduleTaskService.isSendMoneyMessageRunning()) {
            serverStatus = GlobalProperty.ServerStatus.RUNNING
            onOff = GlobalProperty.OnOff.ON
        } else {
            serverStatus = GlobalProperty.ServerStatus.STOP
            onOff = GlobalProperty.OnOff.OFF
        }

        modelAndView.addObject("serverStatus", serverStatus.name)
        modelAndView.addObject("interval", interval.value)
        modelAndView.addObject("onOff", onOff.name)
        modelAndView.addObject("senderNick", senderNick.value)
        modelAndView.addObject("accessToken", accessToken.value)

        modelAndView.addObject("countMessageId", countMessage.id)
        modelAndView.addObject("moneyMessageId", moneyMessage.id)

        modelAndView.addObject("countMessageTitle", countMessage.title)
        modelAndView.addObject("moneyMessageTitle", moneyMessage.title)

        modelAndView.addObject("countMessage", countMessage.message)
        modelAndView.addObject("moneyMessage", moneyMessage.message)

        modelAndView.addObject("countProperty", NumberFormat.getInstance().format(countProperty.value?.toInt()))
        modelAndView.addObject("moneyProperty", NumberFormat.getInstance().format(moneyProperty.value?.toInt()))

        modelAndView.addObject("onCountMessageOff", countMessage.onOff)
        modelAndView.addObject("onMoneyMessageOff", moneyMessage.onOff)

        modelAndView.addObject("countMessageDayBefore", countMessage.dayBefore)
        modelAndView.addObject("moneyMessageDayBefore", moneyMessage.dayBefore)

        modelAndView.addObject("countMessageExcludeNickList", countNickList.sorted().joinToString())
        modelAndView.addObject("moneyMessageExcludeNickList", moneyNickList.sorted().joinToString())

        return Mono.just(modelAndView)
    }
}
