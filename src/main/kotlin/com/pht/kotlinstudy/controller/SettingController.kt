package com.pht.kotlinstudy.controller

import com.pht.kotlinstudy.Global
import com.pht.kotlinstudy.model.*
import com.pht.kotlinstudy.model.dto.GlobalSettingDto
import com.pht.kotlinstudy.model.dto.MessageDto
import com.pht.kotlinstudy.repository.ExcludeNickRepository
import com.pht.kotlinstudy.repository.GlobalPropertyRepository
import com.pht.kotlinstudy.repository.MessageRepository
import com.pht.kotlinstudy.repository.PropertyRepository
import com.pht.kotlinstudy.scheduler.ScheduleTaskService
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.text.NumberFormat
import java.time.Duration
import java.util.*

@RestController
@RequestMapping("setting")
class SettingController(
    val globalPropertyRepository: GlobalPropertyRepository,
    val messageRepository: MessageRepository,
    val propertyRepository: PropertyRepository,
    val excludeNickRepository: ExcludeNickRepository,
    val scheduleTaskService: ScheduleTaskService
) {

    @PostMapping("global/interval")
    fun intervalSetting(@RequestBody request: GlobalSettingDto.Request): GlobalSettingDto.Response {
        var interval = request.interval
        if (interval < 10) {
            interval = 10
        }
        val intervalSetting = globalPropertyRepository.findByKey(Global.KEY_INTERVAL).orElse(GlobalProperty())

        if (interval.toString() != intervalSetting.value) {
            if (scheduleTaskService.isSendCountMessageRunning()) {
                scheduleTaskService.stopSendCountMessageTask()
                scheduleTaskService.startSendCountMessageTask(Duration.ofSeconds(interval))
            }
            if (scheduleTaskService.isSendMoneyMessageRunning()) {
                scheduleTaskService.stopSendMoneyMessageTask()
                scheduleTaskService.startSendMoneyMessageTask(Duration.ofSeconds(interval))
            }
        }

        if (intervalSetting.id == null) {
            intervalSetting.key = Global.KEY_INTERVAL
            intervalSetting.name = Global.KEY_INTERVAL
        }
        intervalSetting.value = Objects.toString(interval)

        globalPropertyRepository.save(intervalSetting)

        val onOff: GlobalProperty.OnOff
        val serverStatus: GlobalProperty.ServerStatus
        if (scheduleTaskService.isSendCountMessageRunning() || scheduleTaskService.isSendMoneyMessageRunning()) {
            serverStatus = GlobalProperty.ServerStatus.RUNNING
            onOff = GlobalProperty.OnOff.ON
        } else {
            serverStatus = GlobalProperty.ServerStatus.STOP
            onOff = GlobalProperty.OnOff.OFF
        }

        return GlobalSettingDto.Response(true, serverStatus, onOff)
    }

    @PostMapping("global/onOff")
    fun onOffSetting(@RequestBody request: GlobalSettingDto.Request): GlobalSettingDto.Response {
        var onOff = request.onOff
        val intervalSetting = globalPropertyRepository.findByKey(Global.KEY_INTERVAL).orElse(GlobalProperty())
        val serverStatusValue = if (onOff == GlobalProperty.OnOff.ON) {
            var isCountRunning = false
            var isMoneyRunning = false
            if (!scheduleTaskService.isSendCountMessageRunning()) {
                val interval = if (intervalSetting.id == null) {
                    30L
                } else {
                    intervalSetting.value?.toLong()
                }
                isCountRunning = scheduleTaskService.startSendCountMessageTask(Duration.ofSeconds(interval as Long))
            }
            if (!scheduleTaskService.isSendMoneyMessageRunning()) {
                val interval = if (intervalSetting.id == null) {
                    30L
                } else {
                    intervalSetting.value?.toLong()
                }
                isMoneyRunning = scheduleTaskService.startSendMoneyMessageTask(Duration.ofSeconds(interval as Long))
            }
            if (isCountRunning || isMoneyRunning) {
                GlobalProperty.ServerStatus.RUNNING
            } else {
                GlobalProperty.ServerStatus.STOP
            }
        } else {
            scheduleTaskService.stopSendCountMessageTask()
            scheduleTaskService.stopSendMoneyMessageTask()
            GlobalProperty.ServerStatus.STOP
        }

        onOff = if (serverStatusValue == GlobalProperty.ServerStatus.RUNNING) {
            GlobalProperty.OnOff.ON
        } else {
            GlobalProperty.OnOff.OFF
        }

        val onOffSetting = globalPropertyRepository.findByKey(Global.KEY_ON_OFF).orElse(GlobalProperty())

        if (onOffSetting.id == null) {
            onOffSetting.key = Global.KEY_ON_OFF
            onOffSetting.name = Global.KEY_ON_OFF
            onOffSetting.value = onOff.name
        }
        onOffSetting.value = onOff.name
        globalPropertyRepository.save(onOffSetting)

        if (intervalSetting.id == null) {
            intervalSetting.key = Global.KEY_INTERVAL
            intervalSetting.name = Global.KEY_INTERVAL
            intervalSetting.value = "30"
            globalPropertyRepository.save(intervalSetting)
        }

        return GlobalSettingDto.Response(true, serverStatusValue, onOff)
    }

    @PostMapping("global/userInfo")
    fun userInfoSetting(@RequestBody request: GlobalSettingDto.UserInfoRequest): GlobalSettingDto.Response {
        val senderNick = request.senderNick
        val token = request.token

        val senderNickProperty = globalPropertyRepository.findByKey(Global.KEY_SENDER_NICK).orElse(GlobalProperty())
        val tokenProperty = globalPropertyRepository.findByKey(Global.KEY_ACCESS_TOKEN).orElse(GlobalProperty())

        if (senderNickProperty.id == null) {
            senderNickProperty.key = Global.KEY_SENDER_NICK
            senderNickProperty.name = Global.KEY_SENDER_NICK
        }
        senderNickProperty.value = senderNick

        globalPropertyRepository.save(senderNickProperty)

        if (tokenProperty.id == null) {
            tokenProperty.key = Global.KEY_ACCESS_TOKEN
            tokenProperty.name = Global.KEY_ACCESS_TOKEN
        }
        tokenProperty.value = token

        globalPropertyRepository.save(tokenProperty)

        val onOff: GlobalProperty.OnOff
        val serverStatus: GlobalProperty.ServerStatus
        if (scheduleTaskService.isSendCountMessageRunning() || scheduleTaskService.isSendMoneyMessageRunning()) {
            serverStatus = GlobalProperty.ServerStatus.RUNNING
            onOff = GlobalProperty.OnOff.ON
        } else {
            serverStatus = GlobalProperty.ServerStatus.STOP
            onOff = GlobalProperty.OnOff.OFF
        }

        return GlobalSettingDto.Response(true, serverStatus, onOff)
    }

    @PostMapping("message")
    fun messageSetting(@RequestBody request: MessageDto.Request): MessageDto.Response {
        val messageValue = request.message
        var propertyValue = request.propertyValue
        val propertyType = request.propertyType
        val title = request.title
        val messageOnOff = request.onOff
        val dayBefore = request.dayBefore
        val excludeNickList = request.excludeNickList

        if (StringUtils.hasText(propertyValue)) {
            propertyValue = NumberFormat.getInstance().parse(propertyValue).toString()
        }

        println("message : $messageValue")
        println("propertyValue: $propertyValue")
        println("propertyType : $propertyType")

        val message = messageRepository.findByKey(Objects.toString(propertyType)).orElse(Message())
        if (message.id == null) {
            val property = Property()
            property.key = Objects.toString(propertyType)
            property.type = propertyType
            property.value = propertyValue
            propertyRepository.save(property)

            message.key = Objects.toString(propertyType)
            message.title = title
            message.message = messageValue
            message.dayBefore = dayBefore
            message.onOff = messageOnOff
            message.addProperty(property)
            messageRepository.save(message)
        } else {
            message.message = messageValue
            message.title = title
            message.dayBefore = dayBefore
            message.onOff = messageOnOff
            message.properties.forEach { property: Property ->
                run {
                    property.value = propertyValue
                    property.type = propertyType
                }
            }
            messageRepository.save(message)
        }

        excludeNickRepository.deleteAllByType(propertyType)
        if (StringUtils.hasText(excludeNickList)) {
            val targetNickList = excludeNickList.trim().split(Regex("\\s*,\\s*")).toHashSet()
            val newNickList = ArrayList<ExcludeNick>()
            for (nick in targetNickList) {
                val newObj = ExcludeNick(nick, propertyType)
                newNickList.add(newObj)
            }
            if (newNickList.isNotEmpty()) {
                excludeNickRepository.saveAll(newNickList)
            }
        }

        val existsExcludeNickList = excludeNickRepository.findAllByType(propertyType)

        val intervalSetting = globalPropertyRepository.findByKey(Global.KEY_INTERVAL).orElse(GlobalProperty())
        val interval = if (intervalSetting.id == null) {
            30L
        } else {
            intervalSetting.value?.toLong()
        }
        val serverOnOffSetting = globalPropertyRepository.findByKey(Global.KEY_ON_OFF).orElse(GlobalProperty())
        if (PropertyType.COUNT == propertyType) {
            if (GlobalProperty.OnOff.ON.name == messageOnOff && serverOnOffSetting.value == GlobalProperty.OnOff.ON.name && !scheduleTaskService.isSendCountMessageRunning()) {
                scheduleTaskService.startSendCountMessageTask(Duration.ofSeconds(interval as Long))
            } else if (GlobalProperty.OnOff.OFF.name == messageOnOff && scheduleTaskService.isSendCountMessageRunning()) {
                scheduleTaskService.stopSendCountMessageTask()
            }
        } else if (PropertyType.MONEY == propertyType) {
            if (GlobalProperty.OnOff.ON.name == messageOnOff && serverOnOffSetting.value == GlobalProperty.OnOff.ON.name && !scheduleTaskService.isSendMoneyMessageRunning()) {
                scheduleTaskService.startSendMoneyMessageTask(Duration.ofSeconds(interval as Long))
            } else if (GlobalProperty.OnOff.OFF.name == messageOnOff && scheduleTaskService.isSendMoneyMessageRunning()) {
                scheduleTaskService.stopSendMoneyMessageTask()
            }
        }

        if (!scheduleTaskService.isSendCountMessageRunning() && !scheduleTaskService.isSendMoneyMessageRunning()) {
            if (serverOnOffSetting.value == GlobalProperty.OnOff.ON.name) {
                serverOnOffSetting.value = GlobalProperty.OnOff.OFF.name
                globalPropertyRepository.save(serverOnOffSetting)
            }
        }

        val serverOnOff: GlobalProperty.OnOff
        val serverStatus: GlobalProperty.ServerStatus
        if (scheduleTaskService.isSendCountMessageRunning() || scheduleTaskService.isSendMoneyMessageRunning()) {
            serverStatus = GlobalProperty.ServerStatus.RUNNING
            serverOnOff = GlobalProperty.OnOff.ON
        } else {
            serverStatus = GlobalProperty.ServerStatus.STOP
            serverOnOff = GlobalProperty.OnOff.OFF
        }

        val existsNickList = existsExcludeNickList.map { e -> e.nick }
        return MessageDto.Response(true, existsNickList.sorted().joinToString(), serverStatus, serverOnOff)
    }
}
