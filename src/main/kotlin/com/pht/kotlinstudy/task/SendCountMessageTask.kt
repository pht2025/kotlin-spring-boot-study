package com.pht.kotlinstudy.task

import com.fasterxml.jackson.databind.ObjectMapper
import com.pht.kotlinstudy.Global
import com.pht.kotlinstudy.model.*
import com.pht.kotlinstudy.repository.ExcludeNickRepository
import com.pht.kotlinstudy.repository.GlobalPropertyRepository
import com.pht.kotlinstudy.repository.MessageRepository
import com.pht.kotlinstudy.repository.SendHistoryRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.util.StopWatch
import org.springframework.web.reactive.function.client.WebClient
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.function.Consumer

class SendCountMessageTask(
    private val messageRepository: MessageRepository,
    private val globalPropertyRepository: GlobalPropertyRepository,
    private val sendHistoryRepository: SendHistoryRepository,
    private val excludeNickRepository: ExcludeNickRepository,
    private var environment: Environment
) : Runnable {

    private val testBaseUrl: String = "http://localhost:8584"
    private val prodBaseUrl: String = "https://pr-api.adventurer.co.kr"
    private val baseUrl: String
    private val getMethod: String = "/v2/user/betCounts"
    private val sendMethod: String = "/v2/message/sendMany"
    private val objectMapper = ObjectMapper()
    private val sendMessageQueue = mutableListOf<Map<String, Any?>>()
    private val historyMap = mutableMapOf<String, SendHistory>()
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private var stopWatch: StopWatch

    init {
        baseUrl = if (environment.activeProfiles.contains("prod")) {
            prodBaseUrl
        } else {
            testBaseUrl
        }
        stopWatch = StopWatch("sendMessage")
        stopWatch.start()
    }

    private fun sendMessage(fromUserNick: String?, betCount: BetCount, message: Message) {
        val toUserId = betCount.userId
        val toUserNick = betCount.userNick
        val existsExcludeNick = excludeNickRepository.existsByNickAndType(toUserNick, PropertyType.COUNT)
        if (existsExcludeNick) {
            return
        }
        val count = Objects.toString(betCount.count)
        val findByToUserIdAndConditionValue = sendHistoryRepository.findByToUserIdAndConditionValue(toUserId, count)
        if (findByToUserIdAndConditionValue?.id == null && (sendMessageQueue.isEmpty() || sendMessageQueue.size < 50)) {
            val sendHistory = SendHistory()
            sendHistory.fromUserNick = fromUserNick
            sendHistory.toUserId = toUserId
            sendHistory.toUserNick = toUserNick
            sendHistory.messageTitle = message.title
            sendHistory.messageContent = message.message
            sendHistory.conditionType = PropertyType.COUNT
            sendHistory.conditionValue = count
            historyMap["${fromUserNick}${toUserId}${count}"] = sendHistory
            sendHistoryRepository.save(sendHistory)

            val formData = HashMap<String, Any?>()
            formData["fromUserNick"] = fromUserNick
            formData["toUserId"] = toUserId
            formData["toUserNick"] = toUserNick
            formData["messageTitle"] = message.title
            formData["messageContent"] = message.message

            sendMessageQueue.add(formData)
        }
    }

    override fun run() {
        if (stopWatch.isRunning) {
            stopWatch.stop()
        }

        val countMessage = messageRepository.findByKey(PropertyType.COUNT.name).orElse(Message())
        val senderNick = globalPropertyRepository.findByKey(Global.KEY_SENDER_NICK).orElse(GlobalProperty())
        val token = globalPropertyRepository.findByKey(Global.KEY_ACCESS_TOKEN).orElse(GlobalProperty())

        val minusDay = countMessage.dayBefore.toLong()

        val senderNickValue = if (environment.activeProfiles.contains("prod")) {
            senderNick.value
        } else {
            "스빠숄뿌뿌"
        }

        val countProperty = countMessage.properties[0]

        val today = LocalDateTime.now(ZoneOffset.UTC);
        val beforeDay = today.minusDays(minusDay)
        val createdAt = beforeDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))

        val url = "${baseUrl}${getMethod}/${countProperty.value}/${createdAt}"
        logger.info("Parsed url : $url")
        val webClient = WebClient.builder()
            .baseUrl(url)
            .defaultHeader("adv-access-token", token.value)
            .build()
        webClient.get()
            .accept(MediaType.APPLICATION_JSON)
            .retrieve().bodyToFlux(BetCount::class.java)
            .collectList()
            .block()?.forEach { betCount -> sendMessage(senderNickValue, betCount, countMessage) }

        if (sendMessageQueue.isNotEmpty()) {
            WebClient.create("${baseUrl}${sendMethod}")
                .post()
                .header("adv-access-token", token.value)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(sendMessageQueue))
                .retrieve().bodyToMono(List::class.java)
                .block()
            sendMessageQueue.clear()
        }

        logger.info("send message : ${stopWatch.totalTimeSeconds}s")
        logger.info("Get method : $getMethod")

        stopWatch = StopWatch("sendMessage")
        stopWatch.start()
    }
}
