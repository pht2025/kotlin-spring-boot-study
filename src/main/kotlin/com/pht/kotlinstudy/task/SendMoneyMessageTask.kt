package com.pht.kotlinstudy.task

import com.fasterxml.jackson.databind.ObjectMapper
import com.pht.kotlinstudy.Global
import com.pht.kotlinstudy.model.*
import com.pht.kotlinstudy.repository.ExcludeNickRepository
import com.pht.kotlinstudy.repository.GlobalPropertyRepository
import com.pht.kotlinstudy.repository.MessageRepository
import com.pht.kotlinstudy.repository.SendHistoryRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.util.StopWatch
import org.springframework.web.reactive.function.client.WebClient
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.function.Consumer

class SendMoneyMessageTask(
    private val messageRepository: MessageRepository,
    private val globalPropertyRepository: GlobalPropertyRepository,
    private val sendHistoryRepository: SendHistoryRepository,
    private val excludeNickRepository: ExcludeNickRepository,
    private var environment: Environment
) : Runnable {

    private val testBaseUrl: String = "http://localhost:8584"
    private val prodBaseUrl: String = "https://pr-api.adventurer.co.kr"
    private val baseUrl: String = if (environment.activeProfiles.contains("prod")) {
        prodBaseUrl
    } else {
        testBaseUrl
    }
    private val getMethod: String = "/v2/user/connectionUserStdBetMoneyList"
    private val sendMethod: String = "/v2/message/sendMany"
    private val objectMapper = ObjectMapper()
    private val sendMessageQueue = mutableListOf<Map<String, Any?>>()
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private var stopWatch: StopWatch

    init {
        stopWatch = StopWatch("sendMessage")
        stopWatch.start()
    }

    private fun sendMessage(fromUserNick: String?, totalStdMoney: TotalStdBetMoney, message: Message) {
        val moneyProperty = message.properties[0]
        val moneyBase = moneyProperty.value?.toLong()!!
        val toUserId = totalStdMoney.userId
        val toUserNick = totalStdMoney.userNick
        val existsExcludeNick = excludeNickRepository.existsByNickAndType(toUserNick, PropertyType.MONEY)
        if (existsExcludeNick) {
            return
        }
        val total = totalStdMoney.totalStdBetMoney
        if (total < moneyBase) {
            return
        }
        val history = sendHistoryRepository.findByToUserIdAndConditionType(toUserId, PropertyType.MONEY)
        var needSendMessage = false
        if (history?.id == null) {
            needSendMessage = true
        } else {
            val today = LocalDate.now().atStartOfDay()
            val lastSendDateTime = history.sendDateTime
            val dayBefore = message.dayBefore
            if (lastSendDateTime.plusDays(dayBefore.toLong()).isBefore(today)) {
                needSendMessage = true
            }
        }
        if (needSendMessage && (sendMessageQueue.isEmpty() || sendMessageQueue.size < 50)) {
            if (history == null) {
                val sendHistory = SendHistory()
                sendHistory.fromUserNick = fromUserNick
                sendHistory.toUserId = toUserId
                sendHistory.toUserNick = toUserNick
                sendHistory.messageTitle = message.title
                sendHistory.messageContent = message.message
                sendHistory.conditionType = PropertyType.MONEY
                sendHistory.conditionValue = total.toString()
                sendHistory.sendDateTime = LocalDate.now().atStartOfDay()
                sendHistoryRepository.save(sendHistory)
            } else {
                history.sendDateTime = LocalDateTime.now()
                sendHistoryRepository.save(history)
            }

            val formData = HashMap<String, Any?>()
            formData["fromUserNick"] = fromUserNick
            formData["toUserId"] = toUserId
            formData["toUserNick"] = toUserNick
            formData["messageTitle"] = message.title
            formData["messageContent"] = message.message

            sendMessageQueue.add(formData)
        }
    }

    override fun run() {
        if (stopWatch.isRunning) {
            stopWatch.stop()
        }

        val moneyMessage = messageRepository.findByKey(PropertyType.MONEY.name).orElse(Message())
        val senderNick = globalPropertyRepository.findByKey(Global.KEY_SENDER_NICK).orElse(GlobalProperty())
        val token = globalPropertyRepository.findByKey(Global.KEY_ACCESS_TOKEN).orElse(GlobalProperty())

        val senderNickValue = if (environment.activeProfiles.contains("prod")) {
            senderNick.value
        } else {
            "스빠숄뿌뿌"
        }

        val url = "${baseUrl}${getMethod}"
        logger.info("Parsed url : $url")
        val webClient = WebClient.builder()
            .baseUrl(url)
            .defaultHeader("adv-access-token", token.value)
            .build()
        webClient.get()
            .accept(MediaType.APPLICATION_JSON)
            .retrieve().bodyToFlux(TotalStdBetMoney::class.java)
            .collectList()
            .block()?.forEach { totalBetMoney -> sendMessage(senderNickValue, totalBetMoney, moneyMessage) }

        if (sendMessageQueue.isNotEmpty()) {
            WebClient.create("${baseUrl}${sendMethod}")
                .post()
                .header("adv-access-token", token.value)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(sendMessageQueue))
                .retrieve().bodyToMono(List::class.java)
                .block()
            sendMessageQueue.clear()
        }

        logger.info("send message : ${stopWatch.totalTimeSeconds}s")
        logger.info("Get method : $getMethod")

        stopWatch = StopWatch("sendMessage")
        stopWatch.start()
    }
}
