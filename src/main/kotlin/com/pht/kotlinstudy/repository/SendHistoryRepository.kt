package com.pht.kotlinstudy.repository

import com.pht.kotlinstudy.model.PropertyType
import com.pht.kotlinstudy.model.SendHistory
import org.springframework.data.repository.CrudRepository

interface SendHistoryRepository : CrudRepository<SendHistory, Long> {

    fun findByToUserIdAndConditionValue(toUserId: String?, count: String?): SendHistory?

    fun findByToUserIdAndConditionType(toUserId: String, type: PropertyType): SendHistory?
}
