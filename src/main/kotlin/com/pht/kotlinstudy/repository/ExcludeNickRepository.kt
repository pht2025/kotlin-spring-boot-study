package com.pht.kotlinstudy.repository

import com.pht.kotlinstudy.model.ExcludeNick
import com.pht.kotlinstudy.model.PropertyType
import org.jetbrains.annotations.NotNull
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface ExcludeNickRepository : JpaRepository<ExcludeNick, Long> {

    fun findByNickAndType(@NotNull nick: String, @NotNull type: PropertyType): Optional<ExcludeNick>

    fun findAllByType(@NotNull type: PropertyType): List<ExcludeNick>

    fun existsByNickAndType(@NotNull nick: String, @NotNull type: PropertyType): Boolean

    @Transactional
    fun deleteAllByType(@NotNull type: PropertyType)
}
