drop table if exists GLOBAL_PROPERTY CASCADE;
drop table if exists MESSAGE CASCADE;
drop table if exists PROPERTY CASCADE;
drop
sequence if exists hibernate_sequence;

create
sequence hibernate_sequence start
with 1 increment by 1;

create table GLOBAL_PROPERTY
(
    id      bigint       not null,
    `key`   varchar(255) not null,
    value   varchar(512) not null,
    name    varchar(255),
    desc    varchar(1024),
    created timestamp,
    updated timestamp,
    primary key (id)
);
create table MESSAGE
(
    id              bigint        not null,
    `key`           varchar(255)  not null,
    title           varchar(255)  not null,
    message         varchar(1024) not null,
    on_off          varchar(3)    not null default 'OFF',
    day_before      int           not null,
    exclude_nick_list varchar(1024),
    created         timestamp,
    updated         timestamp,
    primary key (id)
);
create table PROPERTY
(
    id         bigint        not null,
    `key`      varchar(255)  not null,
    name       varchar(255),
    type       varchar(255),
    value      varchar(1024) not null,
    message_id bigint,
    desc       varchar(255),
    created    timestamp,
    updated    timestamp,
    primary key (id)
);

create table SEND_HISTORY
(
    id              bigint        not null,
    condition_type  varchar(255)  not null,
    condition_value varchar(255)  not null,
    from_user_nick  varchar(255)  not null,
    message_content varchar(1024) not null,
    message_title   varchar(255),
    to_user_id      varchar(255)  not null,
    to_user_nick    varchar(255),
    send_date_time  timestamp,
    created         timestamp,
    updated         timestamp,
    primary key (id, from_user_nick, condition_value)
);

alter table PROPERTY
    add constraint FK2unehkx69go0sj92j1fs22r1w foreign key (message_id) references message;


INSERT INTO GLOBAL_PROPERTY (ID, CREATED, DESC, KEY, NAME, UPDATED, VALUE)
VALUES (1, '2020-06-10 22:00:53.143000', null, 'interval', 'interval', '2020-06-11 02:03:13.306000', '30');
INSERT INTO GLOBAL_PROPERTY (ID, CREATED, DESC, KEY, NAME, UPDATED, VALUE)
VALUES (2, '2020-06-11 23:53:48.967000', null, 'onOff', 'On Off', '2020-06-11 23:53:48.967000', 'OFF');
INSERT INTO GLOBAL_PROPERTY (ID, CREATED, DESC, KEY, NAME, UPDATED, VALUE)
VALUES (3, '2020-06-11 23:53:48.967000', null, 'senderNick', 'Sender Nick', '2020-06-11 23:53:48.967000', '한마음');
INSERT INTO GLOBAL_PROPERTY (ID, CREATED, DESC, KEY, NAME, UPDATED, VALUE)
VALUES (4, '2020-06-11 23:53:48.967000', null, 'accessToken', 'accessToken', '2020-06-11 23:53:48.967000', 'C75283491EE20CD61164C98D8D096A29C3698804010A5F417750BD6CA5B7B760');

INSERT INTO MESSAGE (ID, ON_OFF, DAY_BEFORE, EXCLUDE_NICK_LIST, CREATED, KEY, MESSAGE, TITLE, UPDATED)
VALUES (1, 'OFF', 1, '한마음, 겨울나무, 올리브영', '2020-06-10 22:00:53.165000', 'COUNT', '빠르고 정확한 충환전, 세븐머니!

▷ 500,000 G가 되면 환전 연락주세요
▷ 50만원 결제한도 이상 충전을 원하면 연락주세요

★카톡 : 4989seven
★텔레그램 : vipseven77
  (미이용시 메세지함 → 답장하기 메세지 주세요)

① 문의사항 빠른 응대
② 첫 충전시, 최대 3% 뽀너스머니 추가 지급 이벤트!
③ 최소 충환 500,000 G머니
④ 100,000 G =10,000원 충/환전 시세동일
⑤ 환전 수수료 3% / 충전 수수료 x

★사칭에 주의 하세요

★바로가기 ↓↓↓↓↓
!@#https://sevenmoney1.com', '★★당신이 찾던 ㅎㅈ 세븐머니입니다★★', '2021-05-11 00:08:21.144000');
INSERT INTO MESSAGE (ID, ON_OFF, DAY_BEFORE, EXCLUDE_NICK_LIST, CREATED, KEY, MESSAGE, TITLE, UPDATED)
VALUES (2, 'OFF', 20, '한마음, 겨울나무, 올리브영', '2020-06-10 22:00:53.169000', 'MONEY', '빠르고 정확한 충환전, 세븐머니!

▷ 500,000 G가 되면 환전 연락주세요
▷ 50만원 결제한도 이상 충전을 원하면 연락주세요

★카톡 : 4989seven
★텔레그램 : vipseven77
  (미이용시 메세지함 → 답장하기 메세지 주세요)

① 문의사항 빠른 응대
② 첫 충전시, 최대 3% 뽀너스머니 추가 지급 이벤트!
③ 최소 충환 500,000 G머니
④ 100,000 G =10,000원 충/환전 시세동일
⑤ 환전 수수료 3% / 충전 수수료 x

★사칭에 주의 하세요

★바로가기 ↓↓↓↓↓
!@#https://sevenmoney1.com', '★★당신이 찾던 ㅎㅈ 세븐머니입니다★★', '2021-05-12 00:08:52.006000');

INSERT INTO PROPERTY (ID, CREATED, DESC, KEY, NAME, TYPE, UPDATED, VALUE, MESSAGE_ID)
VALUES (1, '2020-06-10 22:00:53.166000', null, 'COUNT', 'COUNT', 'COUNT', '2020-06-11 23:59:38.773000', '0', 1);
INSERT INTO PROPERTY (ID, CREATED, DESC, KEY, NAME, TYPE, UPDATED, VALUE, MESSAGE_ID)
VALUES (2, '2020-06-10 22:00:53.169000', null, 'MONEY', 'MONEY', 'MONEY', '2020-06-10 22:00:53.169000', '500000', 2);
